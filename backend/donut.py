#process image in donut and get back json

#only for testing(remove afterwards)
# from receipt_generator import dataset

import re
import torch

#load model and processor
def load():
    processor = torch.load("donut_processor")
    model = torch.load("donut_model")
    #use gpu for faster processing (if available)
    device = "cuda" if torch.cuda.is_available() else "cpu"
    model.to(device)
    return processor, model, device

# prepare decoder inputs (taken from donut inference in hugging face)
def processImage(image, processor, model, device):
    task_prompt = "<s_cord-v2>"
    decoder_input_ids = processor.tokenizer(task_prompt, add_special_tokens=False, return_tensors="pt").input_ids
    pixel_values = processor(image, return_tensors="pt").pixel_values

    outputs = model.generate(
        pixel_values.to(device),
        decoder_input_ids=decoder_input_ids.to(device),
        max_length=model.decoder.config.max_position_embeddings,
        early_stopping=True,
        pad_token_id=processor.tokenizer.pad_token_id,
        eos_token_id=processor.tokenizer.eos_token_id,
        use_cache=True,
        num_beams=1,
        bad_words_ids=[[processor.tokenizer.unk_token_id]],
        return_dict_in_generate=True,
    )

    sequence = processor.batch_decode(outputs.sequences)[0]
    sequence = sequence.replace(processor.tokenizer.eos_token, "").replace(processor.tokenizer.pad_token, "")
    sequence = re.sub(r"<.*?>", "", sequence, count=1).strip()  # remove first task start token

    jsonResult = processor.token2json(sequence)
    return jsonResult


#pick up a sample image here (converted to PIL image format) / one at a time
# image = dataset["test"]["image"][1:2] #only for testing

# image = pil_image
# jsonResult = processImage()
# print(jsonResult)


