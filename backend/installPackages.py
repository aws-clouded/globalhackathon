import sys
import subprocess

subprocess.check_call([sys.executable, '-m', 'pip', 'install',
'transformers'])

subprocess.check_call([sys.executable, '-m', 'pip', 'install',
'torch'])

subprocess.check_call([sys.executable, '-m', 'pip', 'install',
'opencv-python'])

subprocess.check_call([sys.executable, '-m', 'pip', 'install',
'datasets'])

subprocess.check_call([sys.executable, '-m', 'pip', 'install',
'flask'])

subprocess.check_call([sys.executable, '-m', 'pip', 'install',
'flask_cors'])
