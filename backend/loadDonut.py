#load pre-trained model
#https://huggingface.co/docs/transformers/main/en/model_doc/donut
#https://huggingface.co/docs/transformers/main/en/model_doc/donut#inference

#pip install missing libraries
from transformers import DonutProcessor, VisionEncoderDecoderModel
from datasets import load_dataset
import torch

#load the model and the image processor
processor = DonutProcessor.from_pretrained("nielsr/donut-demo")
model = VisionEncoderDecoderModel.from_pretrained("nielsr/donut-demo")

torch.save(model, "donut_model")
torch.save(processor, "donut_processor")