import os
import cv2
import donut
from PIL import Image
from flask import Flask, flash, request, redirect, url_for, Blueprint
from werkzeug.utils import secure_filename
import json

receipt_controller = Blueprint('receipt_controller', __name__, template_folder='backend')

#security vulnerability

UPLOAD_FOLDER = "C:/Users/saagi/OneDrive/Desktop/Hackathon/globalhackathon" + "/backend/uploads"
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def getPil(filename):
  file = open(UPLOAD_FOLDER + "/" + filename, "r")
  opencv_image = cv2.imread(UPLOAD_FOLDER + "/" + filename, 1)
  pil_image = Image.fromarray(opencv_image)

  return pil_image



@receipt_controller.route('/receipts', methods=['POST'])
def loadReceipt():
    image = request.files['file']
    if image and allowed_file(image.filename):
      filename = secure_filename(image.filename)
      print(image)
      image.save(os.path.join(UPLOAD_FOLDER, filename))
    
    return json.dumps({'status': 'uploaded'})

@receipt_controller.route('/receipt/<filename>', methods=['GET'])
def returnResult(filename):
  if(filename):
    processor = donut.load()[0]
    model = donut.load()[1]
    device = donut.load()[2]
    result = donut.processImage(getPil(filename), processor, model, device)
    result = processResult(result)
    return result

def processResult(result):
   processed_menu = []
   if(len(result["menu"])==1):
      return result
   for item in result["menu"]:
      if item["price"] == result["total"]["total_price"]:
         continue
      processed_menu.append(item)
   result["menu"] = processed_menu
   return result