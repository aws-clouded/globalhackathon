# Tutorial https://github.com/NielsRogge/Transformers-Tutorials/blob/master/Donut/CORD/Fine_tune_Donut_on_a_custom_dataset_(CORD)_with_PyTorch_Lightning.ipynb
# from datasets import load_dataset
# from ast import literal_eval

# from receipt_controller import resultProcessor

#dataset used to train the donut model 
# dataset = load_dataset("naver-clova-ix/cord-v2")

# example = dataset['train'][0]
# image = example['image']
# # let's make the image a bit smaller when visualizing
# ground_truth = example['ground_truth']

# result = {
#       "menu":[
#          {"cnt":"1","nm":"ChickBrg","price":"9.90"},
#          {"cnt":"1","nm":"Chs Fries","price":"4.95"},
#          {"cnt":"1","nm":"Choc Shake","price":"7.30"},
#          {"cnt":"1","nm":"Amex","price":"22.15"}
#          ],
#          "sub_total":
#            {
#               "service_price":"18.46","subtotal_price":"22.15","tax_price":"22.15"
#            },
#          "total":
#            {
#               "cashprice":"101... 15.","menuqty_cnt":"3.69 = VAT of","total_price":"22.15"
#             }
# }    
# print(resultProcessor(result))

# print(literal_eval(ground_truth)['gt_parse'])
