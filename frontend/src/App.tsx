import { useEffect, useState } from "react";
import "./App.css";
import SelectPhoto from "./components/select-photo";
import UserSelection from "./components/user-selection";
import IdleScreen from "./components/idle-screen";
import Contacts from "./components/contacts";
import { getResult } from "./api/downloadResponse";

function App() {
  // const states = ["idle", "uploadReceipt", "selectParticipants", "splitBill"];
  const [state, setState] = useState(0);
  const [receiptName, setReceipt] = useState("");
  const [receipt, setReceiptData] = useState<any>();
  useEffect(() => {
    getResult(receiptName, setReceiptData);
  }, [receiptName]);
  return (
    <div className="App">
      {state === 0 && <IdleScreen nextState={() => setState(state + 1)} />}
      {state === 1 && (
        <SelectPhoto
          nextState={() => setState(state + 1)}
          setReceipt={setReceipt}
        />
      )}
      {state === 2 && <Contacts nextState={() => setState(state + 1)} />}
      {state === 3 && (
        <UserSelection
          nextState={() => setState(state + 1)}
          receipt={receipt}
        />
      )}
    </div>
  );
}

export default App;
