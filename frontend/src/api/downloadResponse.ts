import axios from "axios";

export const getResult = (file: any, setReceiptData: any) => {
  axios
    .get("http://127.0.0.1:5000/receipt/" + file)
    .then((response) => {
      // Handle successful response
      console.log(response.data);
      setReceiptData(response.data);
      console.log("got json");
    })
    .catch((error) => {
      // Handle error
      console.error(error);
    });
};
