import axios from "axios";

export const uploadImage = (file: any) => {
  const formData = new FormData();
  formData.append("file", file);
  axios
    .post("http://127.0.0.1:5000/receipts", formData)
    .then((response) => {
      // Handle successful response
      console.log(response.data);
      console.log("sent");
    })
    .catch((error) => {
      // Handle error
      console.error(error);
    });
};
