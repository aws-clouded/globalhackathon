import React, { ChangeEvent, useEffect, useState } from "react";
import "./style.css";

export default function Contacts(props) {
  const [filter, setFilter] = useState("");
  const [contacts, setContacts] = useState({
    "John": false,
    "Hannes": false,
    "Alex": false,
    "Craig": false,
    "Ana": false,
    "Jessica": false,
    "Ayela": false,
    "Kirsty": false,
  });
  const [filteredContacts, setFilteredContacts] = useState([]);

  const handleChange = (event, contactKey) => {
    const updatedContacts = {
      ...contacts,
      [contactKey]: event.target.checked,
    };
    setContacts(updatedContacts);
  };

  const handleSearch = (event) => {
    setFilter(event.target.value);
  };

  useEffect(() => {
    if (filter === "") {
      setFilteredContacts(Object.keys(contacts));
    } else {
      setFilteredContacts((prevFilteredContacts) =>
        Object.keys(contacts)
          .filter((contact) => contact.includes(filter))
          .concat(prevFilteredContacts.filter((contact) => contacts[contact]))
      );
    }
  }, [filter, contacts]);

  const onSplit = (e) => {
    e.preventDefault();
    props.nextState();
  };
  return (
    <section className="py-50 px-25" style={{paddingLeft:'8vw', paddingRight:'8vw'}}>
      <form>
        <input
          type="text"
          id="search-bar"
          style={{width: '100%', height:'4vh'}}
          placeholder="Search contacts"
          value={filter}
          onChange={handleSearch}
        />
      </form>
      <form>
        <ul className="flex flex-col gap-25">
          {filteredContacts.map((contact) => (
            <li key={contact} className="flex items-center gap-25 input-group">
           <input
                type="checkbox"
                id={contact}
                checked={contacts[contact]}
                onChange={(e) => handleChange(e, contact)}
              />
            <img
              className="avatar"
              alt="Person1"
              src="/images/guy.jpg"
            />
    
              <div className="avatar-desc">{contact}</div>
              
            </li>
          ))}
        </ul>
        <button style={{backgroundColor: 'blue'}} className="action-btn absolute split-bill-btn-idle" onClick={onSplit}>
          Start Splitting
        </button>
      </form>
    </section>
  );
}
