import React from "react";
import "./style.css";
export default function IdleScreen(props) {
  return (
    <section className="px-25 h-100vh relative pt-100">
      <div className="idle-photo-placeholder mb-50">
      <img className="idle-photo-placeholder mb-50" src="/images/people-eating.jpg"/>
      </div>
      <div>
        <h2 className="mb-25">Scan your bill and split it easily!</h2>
        <p className="mb-12">
          No more phaff with splitting the bill, pay what you owe with Billy.
        </p>
        <p>
          Billy scans your bill, where you can select what you ate and drank,
          settle the payment right away
        </p>
      </div>
      <button
        className="action-btn absolute split-bill-btn-idle"
        onClick={() => props.nextState()}
      >
        <h3>Split the bill</h3>
      </button>
    </section>
  );
}
