import IconButton from "@mui/material/IconButton";
import DriveFolderUploadIcon from "@mui/icons-material/DriveFolderUpload"; // or
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import { useState } from "react";
import { uploadImage } from "../../api/upload";
import UserSelection from "../user-selection";
import ListItem from "@mui/material/ListItem";
import List from "@mui/material/List";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import ListItemText from "@mui/material/ListItemText";
import React, { useEffect } from "react";

const modalStyle = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "100%",
  height: "100%",
  marginTop: "-10px",
  paddingLeft: "40px",
  paddingRight: "40px",
  px: 4,
};

export default function SelectPhoto(props: any) {
  const [file, setFile] = useState<any>();
  const [openReceiptPreview, setOpenReceiptPreview] = useState(false);
  const [openUserSelection, setOpenUserSelection] = useState(false);
  const [response, setResponse] = useState<any>();
  function handleChange(e: React.ChangeEvent<HTMLInputElement>) {
    const selectedFile = e.target.files?.[0];
    if (selectedFile) {
      console.log(selectedFile);
      setFile(URL.createObjectURL(selectedFile));
      uploadImage(selectedFile);
      props.setReceipt(selectedFile.name);
    }
    setOpenReceiptPreview(true);

    props.nextState();
  }

  const getResponse = async () => {
    setOpenUserSelection(true);
    const data = await getResponse();
    setResponse(data);
  };

  useEffect(() => {
    //Process Response and add it to an array
  }, [response]);
  return (
    <section className="px-25 h-100vh relative pt-100">
      <div className="idle-photo-placeholder mb-50">
        <img className="idle-photo-placeholder mb-50" src="/images/pay.jpg" />
      </div>
      <div>
        <h2 className="mb-25">Let's Start!</h2>
        <p className="mb-12">
          If you have a bill with you in person, you can scan it.
        </p>
        <p>
          Alternatively, if you already have the bill in your camera roll, you
          can use that too.
        </p>
      </div>
      <div className="absolute split-bill-btn-idle flex flex-col gap-25">
        <button className="action-btn-transparent">Scan your bill</button>
        <Button
          startIcon={<DriveFolderUploadIcon />}
          variant="contained"
          style={{
            height: "50px",
            borderRadius: "25px",
            borderColor: "#015eb8",
            width: "100%",
          }}
        >
          <input
            accept="image/*"
            id="contained-button-file"
            multiple
            type="file"
            onChange={handleChange}
          />
        </Button>
      </div>
    </section>
  );
}
/*{
  <div className="App">
        {file && (
          <Box sx={{ ...modalStyle, backgroundColor: "white", height: "100%" }}>
            <Modal
              open={openReceiptPreview}
              onClose={() => {
                setOpenReceiptPreview(false);
              }}
              aria-labelledby="modal-modal-title"
              aria-describedby="modal-modal-description"
              sx={{ ...modalStyle, backgroundColor: "white", height: "100%" }}
            >
              <div
                style={{
                  height: "100%",
                  backgroundColor: "white",
                }}
              >
                <List>
                  <ListItem sx={{ backgroundColor: "lightBlue" }}>
                    <ListItemText primary="Uploaded Image" />
                  </ListItem>
                  <ListItem>
                    <ListItemAvatar>
                      <img
                        style={{ width: "5%", height: "10%" }}
                        src={file}
                        alt="Uploaded"
                      />
                    </ListItemAvatar>
                    <ListItemText primary="Person 1" secondary="Total = £30" />
                  </ListItem>
                  <ListItem>
                    <button onClick={() => getResponse()}>
                      User Selection
                    </button>
                  </ListItem>
                </List>

                <Modal
                  open={openUserSelection}
                  onClose={() => {
                    setOpenUserSelection(false);
                  }}
                  aria-labelledby="modal-modal-title"
                  aria-describedby="modal-modal-description"
                  sx={{
                    ...modalStyle,
                    backgroundColor: "white",
                    height: "100%",
                  }}
                >
                  <UserSelection />
                </Modal>
              </div>
            </Modal>
          </Box>
        )}
      </div> 
}*/
