import React from "react";
import Box from "@mui/material/Box";
import Avatar from "@mui/material/Avatar";
import ListItem from "@mui/material/ListItem";
import List from "@mui/material/List";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import ListItemText from "@mui/material/ListItemText";
import Divider from "@mui/material/Divider";
import Checkbox from "@mui/material/Checkbox";
import ListItemButton from "@mui/material/ListItemButton";
import Button from "@mui/material/Button";

function UserSelection(props: any) {
  const [checked, setChecked] = React.useState([1]);

  const handleToggle = (value: number) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };

  const onSave = (e: any) => {
    e.preventDefault();
    props.nextState();
  };

  return (
    <Box sx={{ backgroundColor: "white", width:'100% !important', height: "100%" }}>
      <List sx={{width:'100%'}}>
        <ListItem sx={{ backgroundColor: "darkBlue", color: 'white', marginTop: "-10px", marginRight:'-10px'}}>
          <ListItemAvatar>
            <Avatar
              className="Avatar"
              alt="Person1"
              src="/images/guy.jpg"
            />
          </ListItemAvatar>
          <ListItemText primary="Person 1" sx={{color: 'white',}} />
        </ListItem>

        {[0, 1, 2, 3].map((value) => {
          const labelId = `checkbox-list-secondary-label-${value}`;
          return (
            <Box>
              <ListItem
                key={value}
                secondaryAction={
                  <Checkbox
                    edge="end"
                    onChange={handleToggle(value)}
                    checked={checked.indexOf(value) !== -1}
                    inputProps={{ "aria-labelledby": labelId }}
                  />
                }
                disablePadding
              >
                <ListItemButton>
                  <ListItemText
                    id={labelId}
                    primary={`Line item   £${value + 1}`}
                  />
                </ListItemButton>
              </ListItem>
              <Divider light />
            </Box>
          );
        })}
      </List>
    
        <button style={{backgroundColor: 'blue'}} className="action-btn absolute split-bill-btn-idle" onClick={onSave}>
          Save selections
        </button>

    </Box>
  );
}

export default UserSelection;
